package com.ginf3.em.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the platformuser database table.
 * 
 */
@Entity
@NamedQuery(name="Platformuser.findAll", query="SELECT p FROM Platformuser p")
public class Platformuser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String id;

	private String login;

	private String password;

	public Platformuser() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}