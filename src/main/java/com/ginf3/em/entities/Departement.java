package com.ginf3.em.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the departement database table.
 * 
 */
@Entity
@NamedQuery(name="Departement.findAll", query="SELECT d FROM Departement d")
public class Departement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="departement_id")
	private int departementId;

	@Column(name="departement_code")
	private String departementCode;
	
	private String designation;

	//bi-directional many-to-one association to Employe
	@OneToMany(mappedBy="departement")
	private List<Employe> employes;

	public String getDepartementCode() {
		return departementCode;
	}

	public void setDepartementCode(String departementCode) {
		this.departementCode = departementCode;
	}

	public void setDepartementId(int departementId) {
		this.departementId = departementId;
	}

	public Departement() {
	}

	public Integer getDepartementId() {
		return this.departementId;
	}

	public void setDepartementId(Integer departementId) {
		this.departementId = departementId;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Employe> getEmployes() {
		return this.employes;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public Employe addEmploye(Employe employe) {
		getEmployes().add(employe);
		employe.setDepartement(this);

		return employe;
	}

	public Employe removeEmploye(Employe employe) {
		getEmployes().remove(employe);
		employe.setDepartement(null);

		return employe;
	}

}