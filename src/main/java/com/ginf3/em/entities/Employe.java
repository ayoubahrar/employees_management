package com.ginf3.em.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the employe database table.
 * 
 */
@Entity
@NamedQuery(name="Employe.findAll", query="SELECT e FROM Employe e")
public class Employe implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="employe_id")
	private int employeId;

	@Column(name="employe_code")
	private String employeCode;

	private String adresse;

	private String email;

	private String grade;

	private String nom;

	private String poste;

	private String prenom;

	private BigDecimal sal;

	private String telephone;

	//bi-directional many-to-one association to Departement
	@ManyToOne
	@JoinColumn(name="departement_code", referencedColumnName="departement_code")
	private Departement departement;

	public Employe() {
	}

	

	public int getEmployeId() {
		return employeId;
	}



	public void setEmployeId(int employeId) {
		this.employeId = employeId;
	}



	public String getEmployeCode() {
		return employeCode;
	}



	public void setEmployeCode(String employeCode) {
		this.employeCode = employeCode;
	}



	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPoste() {
		return this.poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public BigDecimal getSal() {
		return this.sal;
	}

	public void setSal(BigDecimal sal) {
		this.sal = sal;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Departement getDepartement() {
		return this.departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

}