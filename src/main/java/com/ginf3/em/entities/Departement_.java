package com.ginf3.em.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-11-03T12:32:59.709+0100")
@StaticMetamodel(Departement.class)
public class Departement_ {
	public static volatile SingularAttribute<Departement, Integer> departementId;
	public static volatile SingularAttribute<Departement, String> designation;
	public static volatile ListAttribute<Departement, Employe> employes;
	public static volatile SingularAttribute<Departement, String> departementCode;
}
