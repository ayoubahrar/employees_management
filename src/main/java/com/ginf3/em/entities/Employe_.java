package com.ginf3.em.entities;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-11-03T12:32:42.635+0100")
@StaticMetamodel(Employe.class)
public class Employe_ {
	public static volatile SingularAttribute<Employe, Integer> employeId;
	public static volatile SingularAttribute<Employe, String> employeCode;
	public static volatile SingularAttribute<Employe, String> adresse;
	public static volatile SingularAttribute<Employe, String> email;
	public static volatile SingularAttribute<Employe, String> grade;
	public static volatile SingularAttribute<Employe, String> nom;
	public static volatile SingularAttribute<Employe, String> poste;
	public static volatile SingularAttribute<Employe, String> prenom;
	public static volatile SingularAttribute<Employe, BigDecimal> sal;
	public static volatile SingularAttribute<Employe, String> telephone;
	public static volatile SingularAttribute<Employe, Departement> departement;
}
