package com.ginf3.em.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.ginf3.em.entities.Employe;

public class EmployeService {
	
	 EntityManager em;
	    EntityTransaction tx;
	    
	    
	     public EmployeService() {
	        EntityManagerFactory emf=Persistence.createEntityManagerFactory("employees_management");
	        em = emf.createEntityManager();
	        tx = em.getTransaction();
	    }
	    
	      public boolean addEmploye (Employe e) {
	        if(SearchEmploye(e.getEmployeCode())!= null)
	            return false;
	        tx.begin();
	        em.persist(e);
	        tx.commit();
	        em.close();
	        System.out.println("employee added");
	        return true;
	        
	    }
	    
	    public boolean updateEmploye(Employe e){
	        if(SearchEmploye(e.getEmployeCode())== null)
	            return false;
	        tx.begin();
	        try {
	            em.merge(e);
	        } catch (Exception ex) {
	            tx.commit();
	            return false;
	        }
	        tx.commit();
	        return true;
	    }
	    
	    public boolean deleteEmploye(Employe e){
	        if(SearchEmploye(e.getDepartement().getDepartementCode())== null)
	            return false;
	        tx.begin();
	        em.remove(e);
	        tx.commit();
	        return true;
	    }
	    public List<Employe> allDept(){
	           EntityManagerFactory emf=Persistence.createEntityManagerFactory("TestJPA2");
	       EntityManager em2= emf.createEntityManager();
	        //tx = em.getTransaction();
	        Query q=em2.createNamedQuery("Select e from Employe e");
	        return q.getResultList();
	    }
	    
	    public Employe SearchEmploye (String s){
	        return em.find(Employe.class,s);
	    }
}
