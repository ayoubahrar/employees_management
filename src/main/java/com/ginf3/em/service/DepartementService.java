package com.ginf3.em.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.ginf3.em.entities.Departement;
import com.ginf3.em.entities.Employe;

public class DepartementService {
	
	EntityManager em;
    EntityTransaction tx;
    
    public DepartementService() {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("employees_management");
        em = emf.createEntityManager();
        tx = em.getTransaction();
    }
    
    public boolean addDepartement (Departement d) {
        if(SearchDept(d.getDepartementCode())!= null)
            return false;
        tx.begin();
        em.persist(d);
        tx.commit();
        return true;
        
    }
    
    public boolean updateDepartement(Departement d){
        if(SearchDept(d.getDepartementCode())== null)
            return false;
        tx.begin();
        try {
            em.merge(d);
        } catch (Exception e) {
            tx.commit();
            return false;
        }
        tx.commit();
        return true;
    }
    
    public boolean deleteDepartemnt(Departement d){
        if(SearchDept(d.getDepartementCode())== null)
            return false;
        tx.begin();
        em.remove(d);
        tx.commit();
        return true;
    }
    public List<Departement> allDept(){
        tx.begin();
        Query q=em.createQuery("Select d from Departement d");
        tx.commit();
        return q.getResultList();
    }
    
    public Departement SearchDept (String s){
        return em.find(Departement.class,s);
    }
    public String DisplayDept(Departement d){
      String s="Departement: "+d.getDepartementCode()+" ,nom: "+d.getDesignation();
        s+="\nLa liste des employés du departement:\n";
        for(Employe e : d.getEmployes()){
            System.out.println("for loop employees");
            s+=e+"\n";
        }
        System.out.println("display employees");
        return s;  
    } 
}
